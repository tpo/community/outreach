# Tor kynningarefni 2019-2020

# 1. TOR FYRIR GAGNALEYND 

### Friðhelgi persónuupplýsinga er mannréttindi

Líkt og mörg okkar, eyðir Aleisha miklu af tímanum sínum á netinu -að hafa samband við vini, pósta á samfélagsmiðla og við vafur á vefnum.

En upp á síðkastið hefur hún tekið eftir að auglýsingar sem tengjast málum sem hún hefur áður leitað að á netinu eru farnar að fylgja henni út um allt á netinu.

Þetta finnst henni vægast sagt uppáþrengjandi, þannig að hún leggst í dálitla rannsóknarvinnu á netinu og kemst fljótt að því að það eru ekki bara auglýsendur sem eru að fylgjast með henni, heldur einnig netþjónustufyrirtækið hennar, ýmis greiningafyrirtæki, samfélagsmiðlafyritæki og fleiri.

Aleisha ákveður að hún vilji reyna að finna og nota hugbúnað sem ekki safnar gögnum um hana, fylgist ekki með henni, og sem gefi ekki öðrum þjónustum upp neinar persónulegar upplýsingar um hana.

Hún fer á námskeið um gagnaleynd í hakkaravinnustofu (hackerspace) í hverfinu sínu og lærir þar um **Tor-vafrann** - eina vefvafrann sem gerir henni hleift að vafra nafnlaust um netið.

---

# 2. TOR FYRIR FEMÍNISTA 

### Framtíðin er fyrir netvædda femínista

Fernanda rekur kvennasamtök sem vinna að réttindum til frjósemisheilbrigði í Brasilíu, þar sem fóstureyðingar eru ólöglegar.

Fernanda og samstarfskonur hennar útbjuggu vefsvæði með upplýsingum um aðgang að fóstureyðingum, getnaðarvörnum og öðrum upplýsingum varðandi barneignir.

Ef þetta vefsvæði væri rakið til þeirra gætu þær átt á hættu að vera handteknar - eða þaðan af verra.

Til að verja sig, útbjuggu Fernanda og samstarfskonur hennar vefsvæðið með Tor **onion-þjónustum**. Onion-þjónustur koma ekki aðeins í veg fyrir að upp komist að þær séu rekstraraðilar vefsvæðisins, heldur hjálpar einnig til við að vernda þá sem heimsækja vefinn, því hann er einungis hægt að skoða með Tor-vafranum. 

Reyndar notar Fernanda **Tor-vafrann** í allt sitt vafur á veraldarvefnum, bara til að hafa öryggið sín megin.

Hún notar einnig Tor-virkjað forrit sem kallast **OnionShare** til að senda skrár til annarra aðgerðasinna, á öruggan hátt og í friði fyrir hnýsni. 

### Aðgerðasinnar eins og Fernanda sem vinna að réttindum til frjósemisheilbrigði eru að berjast fyrir grunn-mannréttindum og frelsi, og Tor styður því við baráttu þeirra.

---

# 3. TOR FYRIR MANNRÉTTINDI

### Vatn er lífið 

Jelani býr í litlu þorpi á bakka stórs fljóts.

Þetta fljót hefur séð samfélaginu fyrir vatni svo lengi sem elstu menn muna.

En núna er fljótinu hans Jelani ógnað af valdamiklum fjölþjóðafyrirtækjum sem eru byrjuð að bora eftir olíu í nágrenninu.

Einkarekin öryggisfyrirtæki, starfandi á vegum þessara fyrirtækja, nota öfluga eftirlitstækni til að fylgjast með netumferð Jelani og nágranna hans í þorpinu, sem eru smátt og smátt að skipuleggja sig til að reyna að verja mikilvæga fljótið þeirra.

Jelani nýtir sér **Tor-vafrann** til að komast hjá því að þessi fyrirtæki fylgist með því þegar hann heimsækir vefsvæði mannréttindasamtaka og lögfræðiaðstoðar eða skrifar bloggfærslur um andófshreyfinguna í þorpinu hans.

Hann notfærir sér líka **OnionShare** og **SecureDrop** til að geta sent skjöl á öruggan hátt til blaðamanna sem eru að hjálpa við að fletta ofan af þeim brotum á mannréttingum sem í gangi eru.

Allur þessi hugbúnaður notar Tor til að vernda friðhelgi Jelanis. 

### Aðgerðasinnar í mannréttindamálum eins og Jelani eru að berjast fyrir réttlátu samfélagi, og Tor styður því við baráttu þeirra.

---

# 4. TOR GEGN RITSKOÐUN

### Byggjum brýr - ekki veggi

Jean var að ferðast í fyrsta skipti til lands langt frá heimahögunum.

Þegar hann kom á hótelið var eitt af því fyrsta sem hann gerði að opna fartölvuna sína.

Hann var það þreyttur að fyrst þegar hann fékk skilaboðin "Tengingin rann út á tíma", þá datt honum í hug að villan stafaði af einhverri vitleysu sem hann hefði sjálfur gert.

En eftir að hafa reynt aftur nokkrum sinnum, þá rann upp fyrir honum að tölvupóstþjónustan hans, fréttavefurinn sem hann var vanur að skoða, ásamt mörgum forritum, virkuðu ekki og voru ekki aðgengileg.

Hann hafði heyrt að internetið væri ritskoðað í þessu landi og velti fyrir sér hvort það væri ástæðan. Hvernig ætti hann að fara að því að hafa samband við fjölskyldu og vini í gegnum þennan ókleifa múr?
Honum tókst samt að framkvæma nokkrar vefleitir of fann spjallsvæði þar sem hann gat fræðst um VPN; einkaþjónustur sem gera kleift að tengjast beint við netkerfi, án ritskoðunar eða annarra hafta.

Jean eyddi hálftíma í að finna út hvaða ódýra VPN-þjónusta væri best.

Han valdi sér eina og í smástund virtist allt vera í lagi, en eftir fimm mínútur rofnaði tengingin og VPN-þjónustan hætti að virka.

Jean hélt áfram að kanna málið og datt loks niður á Tor-vafrann of sá hvernig hann gæti komist í kringum ritskoðun.

Hann fann opinberan vefspegil þar sem hann gat sótt upprunalega útgáfu forritsins.

Þegar hann opnaði **Tor-vafrann**, gat hann fylgt leiðbeiningum fyrir ritskoðaða notendur og tengdist við svokallaða brú sem gerði honum kleift að fá aftur ótakmarkaðan aðgang að intenetinu.

Með Tor-vafranum getur Jean vafrað frjálst og í friði fyrir hnýsni og komist í samband við fjölskylduna sína. 

### Notendur út um víða veröld sem búa við ritskoðun reiða sig á Tor-vafrann sem ókeypis, stöðuga og óritskoðaða leið inn á internetið.

---

# 5. Sameiginlegir hlutar

## Hvað er Tor?

Tor er frjáls hugbúnaður og opið netkerfi sem hjálpar þér að verjast eftirliti, skráningu og ritskoðun á netinu.
Tor er í boði án kvaða frá Tor-verkefninu sem er bandarísk US 501(c)(3) sjálfboðaliðasamtök án gróðamarkmiða. 

Einfaldasta leiðin til að nota Tor er með Tor-vafranum.
Þegar þú notar Tor-vafrann getur enginn séð hvaða vefsvæði þú ert að skoða eða hvar í heiminum þú ert. 

Önnur forrit, eins og SecureDrop og OnionShare, nota Tor til að verja notendur sína gegn eftirliti og ritskoðun.


## 6. Hvernig virkar Tor?

Amal vill heimsækja vefsvæði Bekele án afskipta annarra, þannig að hún ræsir Tor-vafrann.

Tor-vafrinn velur af handahófi rás með þremur endurvörpum, sem eru tölvur víðs vegar um heiminn sem settar eru upp til að beina umferð um Tor-netkerfið.

Tor-vafrinn dulritar síðan í þrígang beiðnir hennar um vefsíður og sendir þær upplýsingar til fyrsta endurvarpans í rásinni hennar.

Fyrsti endurvarpinn fjarlægir fyrsta dulritunarlagið en fær ekki að vita að áfangastaðurinn er vefþjónninn sem hýsir vefsvæði Bekele.

Fyrsti endurvarpinn fær einungis að vita næstu staðsetningu í rásinni, sem er þá endurvarpi númer tvö.

Annar endurvarpinn fjarlægir næsta dulritunarlag og áframsendir beiðnina á þriðja endurvarpann.

Þriðji endurvarpinn fjarlægir síðsta dulritunarlagið og áframsendir vefsíðubeiðnina á áfangastaðinn, en veit ekki að beiðnin kemur frá Amal.

Bekele veit ekki að beiðnin um vefsíðuna kemur frá Amal nema ef hún segir honum frá því sjálf.

## 7. Hverjir nota Tor?

Fólk út um víða veröld notar Tor til að verja persónuupplýsingar sínar og til að geta skoðað vefinn án hafta.

Tor aðstoðar við að vernda blaðamenn, verjendur mannréttinda, fórnarlömb heimilisofbeldis, akademíska vísindamenn og alla þá sem gætu orðið fyrir eftirliti, skráningu og ritskoðun. 

## 6. Af hverju treysta Tor?

Tor er hannað fyrir gagnaleynd. Við vitum ekki hverjir notendur okkar eru, og við höldum ekki neinar skrár yfir virkni notenda.

Þeir sem reka Tor-endurvarpa geta ekki séð raunveruleg auðkenni Tor-notenda.

Sífelld yfirferð jafningja á grunnkóða Tor, sem fram fer í akademískum og frjálsum samfélögum, tryggir að það séu engar bakdyr í Tor, auk þess sem samfélagssáttmálinn okkar lofar að aldrei muni verða neinar bakdyr í Tor. 

## 7. Taktu þátt í Tor-samfélaginu

Tor er gert mögulegt með fjölbreyttri blöndu af notendum, forriturum, endurvörpurum og ráðgjöfum úr hinum ýmsu heimshornum.

Við þörfnumst hjálpar þinnar við að gera Tor ennþá notendavænni og öruggari fyrir fólk allsstaðar í heiminum.

Þú getur boðið fram krafta þína hjá Tor með því að skrifa kóða, reka endurvarpa, útbúa leiðbeiningar, bjóða stuðning við notendur, eða með því að segja fólki sem þú hittir frá Tor.

Samfélaginu sem stendur að Tor er stýrt með siðareglum, og í samfélagssáttmálanum okkar útlistum við þau loforð sem við ætlum aðö standa við gagnvart samfélaginu. 

Lærðu meira um Tor með því að skoða vefsvæðið okkar, wiki-upplýsingasíðurnar, með því að finna okkur á IRC, gerast áskrifandi að einhverjum póstlistanum okkar, eða með því að skrá þig fyrir Tor-fréttabréfinu á at newsletter.torproject.org.


## 8. Sæktu Tor-vafrann

Tor fyrir tölvur
torproject.org/download

TOR Á SNJALLTÆKJUM
### Android 
Tor-vafri fyrir Android er tiltækur á GooglePlay.

### iOS
Onion-vafrinn, þróaður af M. Tigas, er eini vafrinn sem við getum mælt með fyrir iOS.

