# Tor: Material de Divulgação 2019-2020

# 1. TOR PARA PRIVACIDADE

### Privacidade é um direito humano.

Tal como muitos de nós, Aleisha passa grande parte de seu tempo online -- conectando-se a amigos, publicando nas mídias sociais e navegando na web.

Mas, recentemente, ela observou que anúncios publicitários relacionados às suas pesquisas anteriores têm-na perseguido por toda parte em sua vida online.

Ele se sente tão invadida que, ao fazer uma pesquisa online sobre anúncios na internet, descobre que não somente os anunciantes a perseguem, mas também o seu provedor de serviços de internet, as empresas de web analytics, as plataformas de mídias sociais, e muito mais.

Aleisha então decide que deseja encontrar e usar um software que não colete seus dados, não a persiga e não compartilhe com outros serviços nenhum dado privado a seu respeito.

Ela comparece a um treinamento de privacidade em um hackerspace em sua cidade e descobre o "Tor Browser", o único navegador web que a permite navegar anonimamente.

---

# 2.TOR PARA FEMINISTAS

### O futuro é ciberfeminista

Fernanda dirige um coletivo de mulheres sobre direitos reprodutivos no Brasil, país onde o aborto é ilegal.

Fernanda e suas colegas criaram um website com informações sobre acesso a aborto, controle de natalidade e outros recursos para pessoas em busca informação reprodutiva.

Se este site estivesse ligado a eles, eles poderiam ser presos - ou pior.

Para proteger-se, Fernanda e suas colegas criaram seu website usando os **Serviços Onion** do Tor não somente para proteger-se de ser descobertas como as operadoras do servidor, mas também para ajudar a proteger os visitantes de seu website ao tornar obrigatório o uso do Navegador Tor.

De fato, Fernanda usa o **Navegador Tor** para toda sua navegação web, só para garantir sua segurança.

Ela também usa um aplicativo empoderado pelo Tor chamado **OnionShare** para enviar arquivos a outros ativistas com privacidade e segurança.

### Ativistas dos direitos reprodutivos como Fernanda estão lutando por liberdades fundamentais, e o Tor ajuda a empoderar sua resistência.

---

# 3. TOR PARA OS DIREITOS HUMANOS

### Água é vida

Jelani vive em uma pequena aldeia cortada por um rio caudaloso.

Esse rio proveu água para sua comunidade desde os dias de seus ancestrais.

Mas, hoje, o rio de Jelani está ameaçado por poderosas empresas multinacionais que perfuram sua região em busca de petróleo.

Empresas particulares de segurança, pagas por essas empresas, usam poderosos mecanismos de vigilância para monitorar as atividades online de Jelani e seus vizinhos da aldeia que estão se organizando para proteger seu rio sagrado.

Jelani usa o **Navegador Tor** para evitar que essas empresas observem suas visitas a websites internacionais de proteção aos direitos humanos e de auxílio jurídico e escreve artigos em blogs sobre o movimento de resistência em sua aldeia.

Ela também usa o **OnionShare** e o **SecureDrop** para enviar em segurança documentos para jornalistas que estão ajudando a expor essas violações de direitos humanos.

Todos esses softwares usam o Tor para ajudar a proteger a privacidade de Jelani.

### Ativistas de direitos humanos como Jelani lutam por justiça em suas comunidades e o Tor ajuda a empoderar sua resistência.

---

# 4. TOR CONTRA A CENSURA

### Construa pontes, não muros.

Jean viajava pela primeira vez a um país distante de sua família.

Na chegada ao hotel, ele abriu seu laptop.

Ele se sentia tão exausto que quando a mensagem "Conexão expirada" apareceu pela primeira vez em seu navegador web, ele pensou que fosse devida a um erro dele próprio.

Mas, após tentar de novo, e de novo, ele percebeu que seu provedor de email, um website de notícias e muitos aplicativos estavam indisponíveis.

Ele ouvira dizer que esse país censurava a internet e se era isto que estava acontecendo.
Como poderia contactar sua família por trás dessa muralha impenetrável?
Após proceder a algumas pesquisas na web, ele encontrou um fórum onde leu sobre VPNs, serviços de privacidade que permitem que você se conecte a outras redes não censuradas.

Jean gastou cerca de meia hora tentando descobrir qual das opções de VPN de baixo custo era a melhor.

Ele escolheu uma que, por um momento, pareceu funcionar mas, após cinco minutos, a conexão caiu e ele não conseguiu mais conectar-se à VPN.

Jean prosseguiu sua leitura em busca de outras opções e descobriu o Navegador Tor, e suas funcionalidades para contornar a censura.

Ele descobriu um site-espelho oficial para fazer o download do programa.

Quando ele abriu o **Navegador Tor** ele seguiu as instruções para usuários censurados e conectou-se a um ponte que possibilitou novamente seu acesso à internet.

Com o Navegador Tor, Jean pode navegar livremente e com privacidade, entrando em contato com sua família.

### Usuários censurados ao redor de todo o mundo confiam no Navegador Tor como um meio livre, estável e livre de censura para acessar a internet.

---

# 5. Seções compartilhadas

## O que é Tor?

Tor é um software livre e uma rede aberta que ajuda a proteger você contra rastreamento, vigilância e censura online.
Tor é uma criação distribuída gratuitamente por uma organização sem fins lucrativos registrada nos Estados Unidos na categoria 501(c)3 chamada Projeto Tor.

O meio mais fácil de utilizar o Tor é o Navegador Tor
Quando você usa o Navegador Tor, ninguém pode ver quais websites você visita nem em que lugar do mundo você está.

Outros aplicativos tais como SecureDrop ou OnionShare usam o Tor para proteger seus usuários contra vigilância e censura.


## 6. Como funciona o Tor?

Amal quer privacidade para visitar o site de Bekele e, assim, ela abre o Navegador Tor.

O Navegador Tor seleciona um circuito aleatório de três retransmissores, os quais são computadores espalhados ao redor do mundo configurados para rotear todo o seu tráfego pela rede Tor.

O Navegador Tor então criptografa sua solicitação ao website desejado três vezes e a envia ao primeiro retransmissor Tor de seu circuito.

O primeiro retransmissor remove a primeira camada de criptografia mas não sabe que o website de destino é o de Bekele.

O primeiro retransmissor sabe apenas a próxima localização do circuito, que é o segundo retransmissor.

O segundo retransmissor remove outra camada de criptografia e encaminha a solicitação de página web ao terceiro retransmissor.

O terceiro retransmissor remove a última camada de criptografia e encaminha a solicitação de página web ao seu destino, isto é, o website de Bekele, mas não sabe que a solicitação veio de Amal.

Bekele não saberá que a solicitação a seu website veio de Amal, a menos que ela o informe disso.

## 7. Quem usa o Tor?

Pessoas ao redor do mundo usam o Tor para proteger sua privacidade e acessar a web livremente.

Tor ajuda a proteger jornalistas, ativistas de direitos humanos, pesquisadores acadêmicos, bem como qualquer pessoa que experimente rastreamento, censura ou vigilância.

## 6. Por que confiar no Tor?

Tor foi desenvolvido com a privacidade como objetivo. Nós não sabemos quem são nossos usuários e não mantemos registros de atividades de usuários.

Os operadores de retransmissores Tor não conseguem revelar a verdadeira identidade dos usuários do Tor.

A contínua "revisão pelos pares" do código-fonte do Tor por comunidades acadêmicas e de código aberto assegura que não há "portas dos fundos" no Tor, e nosso contrato social promete que jamais criaremos esse tipo de coisa.

## 7. Junte-se à comunidade Tor

O Tor só é possível pela contribuição de um conjunto diversificado de usuários, desenvolvedores, operadores de retransmissores e defensores ao redor do mundo.

Nós precisamos de sua ajuda para tornar o Tor mais usável e seguro para as pessoas em todos os lugares.

Você pode voluntariar-se para ajudar o Tor escrevendo código, operando um retransmissor, criando documentação, oferecendo suporte ao usuário ou contando às pessoas em sua comunidade sobre o Tor.

A comunidade Tor é governada por um código de conduta e nós delineamos nosso conjunto de promessas à comunidade em nosso contrato social.

Saiba mais sobre o Tor visitando nosso website, nossa wiki, localizando-nos no IRC, unindo-se a uma de nossas listas de e-mail ou assinando as "Notícias Tor" no endereço newsletter.torproject.org.


## 8. Baixando o Tor

Tor para Desktop
torproject.org/download

TOR PARA DISPOSITIVOS MÓVEIS
### Android
O Navegador Tor para Android está disponível no GooglePlay.

### iOS
O Navegador Onion, desenvolvido por M. Tigas, é o único navegador que recomendamos para iOS.

